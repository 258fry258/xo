class Game {
    state;
    mode;
    isOverGame;

    constructor() {
        this.state = [[null, null, null], [null, null, null], [null, null, null]];
        this.mode = 'x';
        this.isOverGame = false;

    }
   setMode(){
        if (!this.isOverGame){
            if (this.mode === 'x'){
                this.mode = 'o';
            }
            else this.mode = 'x';
        }
    }
    examination () {
        let result = undefined;

        let i = 0;
        while (i <= 2) {

            if (this.state[i][0] === this.state[i][1] && this.state[i][1] === this.state[i][2]) {
                result = this.state[i][0];
                if (result != null) {
                    this.isOverGame = true;
                }
            }
            i++;
        }
        let j = 0;
        while (j <= 2) {
            if (this.state[0][j] === this.state[1][j] && this.state[1][j] === this.state[2][j]) {
                result = this.state[0][j];
                if (result != null) {
                    this.isOverGame = true;
                }
            }
            j++;
        }
        if ((this.state[0][0] === this.state[1][1] && this.state[1][1] === this.state[2][2]) || (this.state[0][2] === this.state[1][1] && this.state[1][1] === this.state[2][0])) {
            result = this.state[1][1];
            if (result != null) {
                this.isOverGame = true;
            }
        }

        else if (result === undefined) {
            this.isOverGame = true;
            alert('ничья');
        }
    }
    getGameFieldStatus(){
        var message = "";

        for (var i = 0; i < this.state.length; i++) {
            for (var j = 0; j < this.state.length; j++) {
                message += this.state[i][j] + " ";
            }
            message += "\n";
        }

        alert(message);
    }
    fieldCellValue (row, col){
        if (this.state[row][col] != null) {
            alert('Поле занято');
            return;
        }
        else {
            this.state[row][col] = this.mode;
            this.examination();
        }
        if (this.isOverGame !== true){
            this.setMode();
        }
        else{
            alert(`Победили ${this.mode}`);
        }
    }
}

const game = new Game();
while(game.isOverGame !== true){
    game.getGameFieldStatus();
    alert(`ходят ${game.mode}`);
    const row = parseInt(prompt("Введите номер строки (от 0 до 2):"));
    const col = parseInt(prompt("Введите номер столбца (от 0 до 2):"));
    game.fieldCellValue(row, col);
}
